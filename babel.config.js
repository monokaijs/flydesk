module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    ['module-resolver', {
      "alias": {
        "@components": "./src/components",
        "@configs": "./src/configs",
        "@navigations": "./src/navigations",
        "@hooks": "./src/hooks",
        "@redux": "./src/redux",
        "@assets": "./src/assets",
        "@screens": "./src/screens"
      }
    }]
  ],
};
