import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { BottomTabsNavigation } from "./navigations/BottomTabsNavigation";

const App = () => {
  return (
    <NavigationContainer>
      <BottomTabsNavigation/>
    </NavigationContainer>
  )
}
export default App;
